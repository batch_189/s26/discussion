/*
 SQL- data store in table-based
    - vertically scalable
    - better for multi-row transactions

    - modern relational databases like "PostgreSQL" or "MySQL" implement transactions with ACID.
    - ACID - Atomicity, Consistency, Isolation, Durability
        - Atomicity - 

 NoSQL- one of noSQL is mongoDB
    - horizontally scalable
    - databases are documents
    - better for unstructured data like documents or JSON.

    MongoDB - part of the word humongus (huge or enormous)
            - database that uses key-values pairs
        
        # Relational Database vs Non-Relational Database
            - from tables to collection
            - from rows to documents
            - from columns to fields

Data Models
    -Users
        - have both information of customer and admin
            -users : name, last name, email, password, mobile number, enrollments
            -admin : boolean
    -Courses
        - name, description, price, is active?, slots, enrollees

    -Transactions
        - User id, course id, is paid?, payment method

Relationships Between Data
    One-to-one 
        e.g. A person ca only have one employee ID  on a given company.
        -TIN, Gov. ID etc..
    
    One-to-many
        e.g A person can have one or more email address.
        -  courses

    Many-to-many
        e.g A book can be written by multiple authors and an author can write
            multiple books.

Translating data models into a ERD

    ERD - Entity-relationship diagram
        - is a diagram used 
        - aims to show what attributes (fields )an entity (collections) has it. 
            also show the relationship of attribute bet entities.
        - used for SQL and relational databases.

    NoSQL - no joining tables that connect multiple tables together, they would normally have
    arrays with list of unique ids of other obj that connect them.

 */

